<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Типография FastPrint</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<header>
    <div class="header-logo"></div>
    <div class="header-heading"><h1>Типография FastPrint</h1></div>
    <div></div>
</header>

<main>
    <div id="menu">
        <div>
            <?php
            if (isset($_COOKIE["user"])) {
                echo '<a href="authentication/exit.php">Выйти</a>';
            } else {
                echo '
                        <a href="?authentication=enter" data-button-to-call-modal="modal-to-authorization">Войти</a>
                        <a href="?authentication=registration" data-button-to-call-modal="modal-to-registration">Зарегистрироваться</a>
                        ';
            }
            ?>
        </div>
    </div>

    <?php

    if (isset($_COOKIE["user"])) {
        echo '<h2>Здравствуйте, '.$_COOKIE["user"].'</h2>';
//        echo '<a href="authentication/exit.php">Выйти</a>';
    } else {
        if (isset($_GET['authentication'])){
            if ($_GET['authentication'] == 'enter' ) {
                echo '
<form class="auth" action="authentication/auth.php" method="post">
	<h2>Авторизация</h2>
	<label for="login">Логин</label>
    <input id="login" type="text" placeholder="введите логин" name="login">
    <label for="password">Пароль</label>
    <input type="password" placeholder="введите пароль" name="pass">
    <input id="password" type="submit" value="Авторизоваться">
</form>';
            } else if ($_GET['authentication'] == 'registration' ){
                echo '
        <form class="auth" action="authentication/registration.php" method="post">
	<h2>Зарегистрироваться</h2>
	<label for="login">Логин</label>
    <input id="login" type="text" placeholder="введите логин" name="login">
    <label for="password">Пароль</label>
    <input type="password" placeholder="введите пароль" name="pass">
    <input id="password" type="submit" value="Зарегистрироваться">
    </form>';
            }
        }

    }

    if (empty($_POST)) {
        include 'calcform.php';
    } else {
        include 'calculate.php';
        echo '
    <a href="/exam/index.php">Рассчитать заново</a>
    ';
    }
    ?>
</main>


<footer>

</footer>

</body>
</html>