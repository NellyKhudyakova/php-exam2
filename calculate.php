<?php
$size_of_A2 = 594 * 420;

$format_entered = htmlspecialchars($_POST['format']);
$chromaticity_entered = htmlspecialchars($_POST['chromaticity']);
$density_entered = htmlspecialchars($_POST['density']);
$density_entered = ltrim($density_entered, 'd');
$quantity_lists_entered = htmlspecialchars($_POST['quantity']);
$result = 'undef';

//----------------------------------------------------------------------------------------------------

$mysqli = mysqli_connect('localhost', 'root', '', 'fastprint');
mysqli_set_charset($mysqli, "utf8");

if (mysqli_connect_errno()) {
    echo 'Ошибка подключения к БД: ' . mysqli_connect_error();
} else {
    mysqli_set_charset($mysqli, "utf8");

    $sql = mysqli_query($mysqli, 'SELECT * FROM optionalparams');
//    $sql_res = mysqli_query($mysqli, $sql);
    $row = mysqli_fetch_assoc($sql);
//$type = $row['tipe'];
//echo $type;
//    print_r($row);

    $setofprintforms = $row['setofprintforms'];
    $imprint = $row['imprint'];
    $extraprice = $row['extraprice'];
    $makeready_human = $row['makeready'];

    $sql = mysqli_query($mysqli, 'SELECT * FROM storage');
//    $sql_res = mysqli_query($mysqli, $sql);


//    print_r($row);

    $storage_info = [];
//    $counter = 0;

    while ($row = mysqli_fetch_assoc($sql)){

//        $storage_info[$counter]['paper_id'] = $row['id'];
        $counter = $row['id'];

        $storage_info[$counter] = [];
        $storage_info[$counter]['width'] = $row['width'];
        $storage_info[$counter]['length'] = $row['length'];
        $storage_info[$counter]['quantity_paper_in_package'] = $row['sheets'];
        $storage_info[$counter]['weight']= $row['weight'];
        $storage_info[$counter]['density'] = $row['density'];
        $storage_info[$counter]['bundlesofpaper'] = $row['bundlesofpaper'];
        $storage_info[$counter]['costperkg'] = $row['costperkg'];
        $counter++;
    }

//    print_r($storage_info);

//---------------------------------------------------------------------------------------------------------------------------------

    for ($i = 0; $i < 8; $i++){
        if (!isset($storage_info[$i])) continue;
        if ($storage_info[$i]['density'] == $density_entered){

            $width = $storage_info[$i]['width'];
            $length = $storage_info[$i]['length'];
            $quantity_paper_in_package = $storage_info[$i]['quantity_paper_in_package'];
            $weight = $storage_info[$i]['weight'];
            $density = $storage_info[$i]['density'];
            $bundles_of_paper = $storage_info[$i]['bundlesofpaper'];
            $price_per_kg = $storage_info[$i]['costperkg'];
        }
    }
//    echo $price_per_kg.'<br>';

    if (!isset($width)) {
        $result = 'нет требуемого вида бумаги';
    } else {
        $size_of_page = $width * $length;
        $amount_cof = floor($size_of_page / $size_of_A2);
        $quantity_paper_total = $quantity_paper_in_package * $amount_cof * $bundles_of_paper;

        if ($format_entered === 'A5') {
            $list_on_paper = 8;
        } else if ($format_entered === 'A4') {
            $list_on_paper = 4;
        } else if ($format_entered === 'A3') {
            $list_on_paper = 2;
        } else if ($format_entered === 'A2') {
            $list_on_paper = 1;
        }

        $quantity_paper_needed = $quantity_lists_entered / $list_on_paper;

//        echo 'quantity_paper_needed = '.$quantity_paper_needed.'<br>';
//        echo '$quantity_paper_total  = '.$quantity_paper_total .'<br>';

        if ($quantity_paper_total < $quantity_paper_needed) {
            $result = 'недостаточно требуемого вида бумаги';
        } else {
            $quantity_folders_needed = ceil($quantity_paper_needed / $quantity_paper_in_package);

            $price_without_makeready = $weight * $price_per_kg * $extraprice * $imprint * $setofprintforms;
//            echo $weight.'<br>';
//            echo $price_per_kg .'<br>';
//            echo $extraprice.'<br>';
//            echo $imprint.'<br>';
//            echo $setofprintforms.'<br>';
            if ($chromaticity_entered === '4+4' || $chromaticity_entered === '4+1') {
                $price_without_makeready *= 2;
            }


            $price_makeready_machine = ceil($quantity_paper_needed / 200) * $weight * $price_per_kg + $makeready_human;
            $result = $price_without_makeready + $price_makeready_machine;
        }

    }
}
$mysqli = mysqli_connect('localhost', 'root', '', 'fastprint');
mysqli_set_charset($mysqli, "utf8");

if (mysqli_connect_errno()){
    echo 'Ошибка подключения к БД: ' . mysqli_connect_error();
} else {
    $sql_res = mysqli_query($mysqli, 'INSERT INTO orders VALUES ("", "'.$format_entered.'", "'.$chromaticity_entered.'", "'.$density_entered.'", "'
        .$quantity_lists_entered.'", "'.$format_entered.'", "'.$result.'", "sent in process")');
}
//echo '<h1>'.$result.'</h1>';
echo ' <div id="calc_result">
 <h2>Рассчетная стоимость</h2>
<div>Формат - '.$format_entered.'</div>
<div>Цветность - '.$chromaticity_entered.'</div>
<div>Плотность - '.$density_entered.'</div>
<div>Тираж - '.$quantity_lists_entered.'</div>
<div>Количество упаковок бумаги - '.$quantity_folders_needed.'</div>
<div>Вес одной упаковки - '.$weight.'</div>
<div>Стоимость за кг бумаги - '.$price_per_kg.'</div>
<div>Стоимость одного оттиска - '.$imprint.'</div>
<div>Стоимость комплекта печатных форм - '.$setofprintforms.'</div>
<div>Стоимость приладки - '.$price_makeready_machine.'</div>
<div>Наценка типографии - '.$extraprice.'</div>
<div><strong>Итого: '.$result.'</strong></div>
</div>

';



