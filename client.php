<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Типография FastPrint</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<header>
    <div class="header-logo"></div>
    <div class="header-heading"><h1>Типография FastPrint</h1></div>
    <div></div>
</header>

<main>
    <div id="menu">
        <div>
            <?php
            if (isset($_COOKIE["user"])) {
                echo '<a href="authentication/exit.php">Выйти</a>';
            }
            ?>
        </div>
    </div>

    <?php

    if (isset($_COOKIE["user"])) {
        echo '<h2>Здравствуйте, '.$_COOKIE["user"].'</h2>';
    }
    if (empty($_POST)) {
        include 'calcform.php';
    } else {
        include 'calculate.php';
        echo '
    <a href="#">Оформить заказ</a>
    <a href="/exam/index.php">Рассчитать заново</a>
    ';
    }

//    if (isset($_GET['authentication'])&&$_GET['authentication']='processing') {
//        $mysqli = mysqli_connect('localhost', 'root', '', 'fastprint');
//        mysqli_set_charset($mysqli, "utf8");
//
//        if (mysqli_connect_errno()){
//            echo 'Ошибка подключения к БД: ' . mysqli_connect_error();
//        } else {
//            $sql_res = mysqli_query($mysqli, 'INSERT INTO orders VALUES ("", "'.$format_entered.'", "'.$chromaticity_entered.'", "'.$density_entered.'", "'
//                .$quantity_lists_entered.'", "'.$format_entered.'", "'.$result.'", "Отправлено в работу")');
//        }
//        if (mysqli_errno($mysqli)){
//            echo '<div class="error">Запись не добавлена ' . mysqli_error($mysqli) .'</div>';
//
//        } else
//            echo '<div class="ok">Запись добавлена</div>';
//    }
    ?>
</main>


<footer>

</footer>

</body>
</html>