<div>
    <h2>Рассчитать стоимость печати</h2>
    <form action="#" method="post">
        <label> Формат:
            <select name="format">
                <option value="A5">A5</option>
                <option value="A4">A4</option>
                <option value="A3">A3</option>
                <option value="A2">A2</option>
            </select>
        </label>
        <label>
            Цветность:
            <select name="chromaticity">
                <option value="4+4">4+4</option>
                <option value="4+1">4+1</option>
                <option value="4+0">4+0</option>
                <option value="1+0">1+0</option>
            </select>
        </label>
        <label> Плотность:
            <select name="density">
                <option value="d80">80</option>
                <option value="d300">300</option>
            </select>
        </label>
        <label>
            Тираж
            <input name="quantity" type="number"/>
        </label>
        <input type="submit">
    </form>
</div>